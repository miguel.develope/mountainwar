﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MountainWar.Core.Interfaces;
using MountainWar.Core.Views;

namespace MountainWar.Controllers
{
  [Produces("application/json")]
  [Route("game")]
  public class GameController : Controller
  {
    private readonly IServiceGame service;

    public GameController(IServiceGame _service)
    {
      service = _service;
    }

    [Authorize]
    [HttpPut]
    [Route("gameover/{id}")]
    public string GameOver(string id)
    {
      return service.GameOver(id);
    }

    [Authorize]
    [HttpGet]
    [Route("listgames/{iduser}")]
    public List<ViewGame> ListGames(string iduser)
    {
      return service.ListGames(iduser);
    }

    [Authorize]
    [HttpPost]
    [Route("newgame")]
    public string NewGame([FromBody]ViewNewGame view)
    {
      return service.NewGame(view);
    }

    [Authorize]
    [HttpPut]
    [Route("savegame/{id}")]
    public string SaveGame([FromBody]ViewNewGame view, string id)
    {
      return service.SaveGame(view, id);
    }

    [Authorize]
    [HttpPut]
    [Route("endmissiongame/{id}")]
    public string EndMissionGame([FromBody]ViewNewGame view, string id)
    {
      return service.EndMissionGame(view, id);
    }

    [Authorize]
    [HttpPut]
    [Route("updategame/{id}")]
    public string UpdateGame([FromBody]ViewNewGame view, string id)
    {
      return service.UpdateGame(view, id);
    }

  }
}