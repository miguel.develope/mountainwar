﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MountainWar.Core.Enumns;
using MountainWar.Core.Interfaces;
using MountainWar.Core.Views;

namespace MountainWar.Controllers
{
  [Produces("application/json")]
  [Route("character")]
  public class CharacterController : Controller
  {
    private readonly IServiceCharacter service;
    public CharacterController(IServiceCharacter _service)
    {
      service = _service;
    }

    [Authorize]
    [HttpPost]
    [Route("additens/{idcharacter}/{iduser}")]
    public string AddItens([FromBody]ViewAddItens view, string idcharacter, string iduser)
    {
      return service.AddItens(view.Item, idcharacter, iduser);
    }

    [Authorize]
    [HttpPost]
    [Route("addspells/{idcharacter}/{iduser}")]
    public string AddSpells([FromBody]ViewAddSpells view, string idcharacter, string iduser)
    {
      return service.AddSpells(view.Spell, idcharacter, iduser);
    }

    [Authorize]
    [HttpPut]
    [Route("deadcharacter/{iduser}")]
    public string DeadCharacter(string iduser)
    {
      return service.DeadCharacter(iduser);
    }

    [Authorize]
    [HttpPut]
    [Route("downlife/{idcharacter}/{iduser}")]
    public decimal DownLife([FromBody]ViewSetValueDec value, string idcharacter, string iduser)
    {
      return service.DownLife(value.Value, idcharacter, iduser);
    }

    [Authorize]
    [HttpPut]
    [Route("downmana/{idcharacter}/{iduser}")]
    public decimal DownMana([FromBody]ViewSetValueDec value, string idcharacter, string iduser)
    {
      return service.DownMana(value.Value, idcharacter, iduser);
    }

    [Authorize]
    [HttpPut]
    [Route("downmoney/{idcharacter}/{iduser}")]
    public decimal DownMoney([FromBody]ViewSetValueDec value, string idcharacter, string iduser)
    {
      return service.DownMoney(value.Value, idcharacter, iduser);
    }

    [Authorize]
    [HttpPut]
    [Route("downpower/{idcharacter}/{iduser}")]
    public decimal DownPower([FromBody]ViewSetValueDec value, string idcharacter, string iduser)
    {
      return service.DownPower(value.Value, idcharacter, iduser);
    }

    [Authorize]
    [HttpPut]
    [Route("downresistance/{idcharacter}/{iduser}")]
    public decimal DownResistance([FromBody]ViewSetValueDec value, string idcharacter, string iduser)
    {
      return service.DownResistance(value.Value, idcharacter, iduser);
    }

    [Authorize]
    [HttpGet]
    [Route("getcharacter/{iduser}")]
    public ViewCharacter GetCharacter(string iduser)
    {
      return service.GetCharacter(iduser);
    }

    [Authorize]
    [HttpGet]
    [Route("listcharacters")]
    public List<ViewCharacter> ListCharacters()
    {
      return service.ListCharacters();
    }

    [Authorize]
    [HttpPost]
    [Route("newcharacter/{id}")]
    public string NewCharacter([FromBody]ViewCreateCharacter view, string id)
    {
      return service.NewCharacter(view, id);
    }

    [Authorize]
    [HttpPut]
    [Route("updatecharacter/{iduser}/{idcharacter}")]
    public string UpdateCharacter([FromBody]ViewCreateCharacter view, string iduser, string idcharacter)
    {
      return service.UpdateCharacter(view, iduser, idcharacter);
    }

    [Authorize]
    [HttpPut]
    [Route("updatenumberstrikes/{idcharacter}/{iduser}")]
    public int UpdateNumberStrikes(string idcharacter, string iduser)
    {
      return service.UpdateNumberStrikes(idcharacter, iduser);
    }

    [Authorize]
    [HttpPut]
    [Route("uplife/{idcharacter}/{iduser}")]
    public decimal UpLife([FromBody]ViewSetValueDec value, string idcharacter, string iduser)
    {
      return service.UpLife(value.Value, idcharacter, iduser);
    }

    [Authorize]
    [HttpPut]
    [Route("upmana/{idcharacter}/{iduser}")]
    public decimal UpMana([FromBody]ViewSetValueDec value, string idcharacter, string iduser)
    {
      return service.UpMana(value.Value, idcharacter, iduser);
    }

    [Authorize]
    [HttpPut]
    [Route("upmoney/{idcharacter}/{iduser}")]
    public decimal UpMoney([FromBody]ViewSetValueDec value, string idcharacter, string iduser)
    {
      return service.UpMoney(value.Value, idcharacter, iduser);
    }

    [Authorize]
    [HttpPut]
    [Route("uppower/{idcharacter}/{iduser}")]
    public decimal UpPower([FromBody]ViewSetValueDec value, string idcharacter, string iduser)
    {
      return service.UpPower(value.Value, idcharacter, iduser);
    }

    [Authorize]
    [HttpPut]
    [Route("upresistance/{idcharacter}/{iduser}")]
    public decimal UpResistance([FromBody]ViewSetValueDec value, string idcharacter, string iduser)
    {
      return service.UpResistance(value.Value, idcharacter, iduser);
    }

    [Authorize]
    [HttpPut]
    [Route("price/{idcharacter}/{iduser}")]
    public string Price([FromBody]ViewPrice view, string idcharacter, string iduser)
    {
      return service.Price(view.Type, iduser, idcharacter);
    }

  }
}