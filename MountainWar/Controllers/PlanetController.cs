﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MountainWar.Core.Bussiness;
using MountainWar.Core.Enumns;
using MountainWar.Core.Interfaces;
using MountainWar.Core.Views;

namespace MountainWar.Controllers
{
  [Produces("application/json")]
  [Route("planet")]
  public class PlanetController : Controller
  {
    private readonly IServicePlanet service;
    public PlanetController(IServicePlanet _service)
    {
      service = _service;
    }

    [Authorize]
    [HttpGet]
    [Route("listplanets")]
    public List<Planet> ListPlanets()
    {
      return service.ListPlanets();
    }

    [Authorize]
    [HttpPost]
    [Route("newplanet")]
    public string NewPlanet([FromBody]ViewCreatePlanet view)
    {
      return service.NewPlanet(view);
    }

    [Authorize]
    [HttpPut]
    [Route("updateplanet/{type}")]
    public string UpdatePlanet([FromBody]ViewCreatePlanet view, EnumTypePlanet type)
    {
      return service.UpdatePlanet(view, type);
    }
  }
}
