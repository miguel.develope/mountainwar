﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using MountainWar.Core.Interfaces;
using MountainWar.Core.Views;

namespace MountainWar.Controllers
{
  [Produces("application/json")]
  [Route("authentication")]
  public class AuthenticationController : Controller
  {
    private readonly IServiceAuthentication service;
    private const string Secret = "db3OIsj+BXE9NZDy0t8W3TcNekrF+2d/1sFnWG4HnV8TZY30iTOdtVWJG8abWvB1GlOgJuQZdcF2Luqm/hccMw==";

    public AuthenticationController(IServiceAuthentication _service)
    {
      service = _service;
    }

    [AllowAnonymous]
    [HttpPost]
    public ObjectResult Post([FromBody]ViewAuthentication view)
    {
      if (String.IsNullOrEmpty(view.Mail))
        return BadRequest("MSG1");
      if (String.IsNullOrEmpty(view.Password))
        return BadRequest("MSG2");

      var user = this.service.Authentication(view.Mail, view.Password);


      var claims = new[]
      {
        new Claim(ClaimTypes.Name, user.Name),
        new Claim(ClaimTypes.Email, user.Mail),
        new Claim(ClaimTypes.UserData, user.IdUser)
      };

      var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secret));
      var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

      var token = new JwtSecurityToken(
          issuer: "localhost",
          audience: "localhost",
          claims: claims,
          expires: DateTime.Now.AddYears(1),
          signingCredentials: creds
      );

      var tokenId = new JwtSecurityTokenHandler().WriteToken(token);
      user.Token = tokenId;

      return Ok(user);
    }


  }
}