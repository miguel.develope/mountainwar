﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MountainWar.Core.Bussiness;
using MountainWar.Core.Interfaces;
using MountainWar.Core.Views;

namespace MountainWar.Controllers
{
  [Produces("application/json")]
  [Route("mission")]
  public class MissionController : Controller
  {
    private readonly IServiceMission service;
    public MissionController(IServiceMission _service)
    {
      service = _service;
    }

    [Authorize]
    [HttpGet]
    [Route("getmission/{id}")]
    public ViewListMission GetMission(string id)
    {
      return service.GetMission(id);
    }

    [Authorize]
    [HttpGet]
    [Route("getmissions")]
    public List<ViewListMission> GetMissions()
    {
      return service.GetMissions();
    }

    [Authorize]
    [HttpPost]
    [Route("newmission")]
    public string NewMission([FromBody]ViewMissionCreate view)
    {
      return service.NewMission(view);
    }

    [Authorize]
    [HttpPut]
    [Route("updatemission/{number}")]
    public string UpdateMission([FromBody]ViewMissionCreate view, int number)
    {
      return service.UpdateMission(view, number);
    }

    [Authorize]
    [HttpPut]
    [Route("updatemissionsize/{number}")]
    public string UpdateMissionSize([FromBody]ViewMissionCreate view, int number)
    {
      return service.UpdateMissionSize(view, number);
    }

    [Authorize]
    [HttpGet]
    [Route("getmessagedialog/{mission}/{dialog}")]
    public string GetMessageDialog(int mission, int dialog)
    {
      return service.GetMessageDialog(mission, dialog);
    }

    [Authorize]
    [HttpGet]
    [Route("getcharacteres/{mission}")]
    public List<ViewListCharacter> GetCharacteres(int mission)
    {
      return service.GetCharacteres(mission);
    }
  }
}