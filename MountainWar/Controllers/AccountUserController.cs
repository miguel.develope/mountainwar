﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MountainWar.Core.Bussiness;
using MountainWar.Core.Interfaces;
using MountainWar.Core.Views;

namespace MountainWar.Controllers
{
  [Produces("application/json")]
  [Route("accountuser")]
  public class AccountUserController : Controller
  {
    private readonly IServiceAccountUser service;

    public AccountUserController(IServiceAccountUser _service)
    {
      service = _service;
    }

    [HttpPost]
    [Route("newaccount")]
    public string NewAccount([FromBody]ViewCreateAccount view)
    {
      return service.NewAccount(view);
    }

    [Authorize]
    [HttpPost]
    [Route("alteraccount/{id}")]
    public string AlterAccount([FromBody]ViewCreateAccount view, string id)
    {
      return service.AlterAccount(view, id);
    }

    [Authorize]
    [HttpPost]
    [Route("alterpassword/{id}")]
    public string AlterPassword([FromBody]ViewCreateAccount view, string id)
    {
      return service.AlterPassword(view, id);
    }

  }
}