﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MountainWar.Core.Interfaces;
using MountainWar.Data;
using MountainWar.Service.Auth;
using MountainWar.Service.Specific;
using Tools;

namespace MountainWar
{
    public class Startup
    {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }
    private const string Secret = "db3OIsj+BXE9NZDy0t8W3TcNekrF+2d/1sFnWG4HnV8TZY30iTOdtVWJG8abWvB1GlOgJuQZdcF2Luqm/hccMw==";
    // This method gets called by the runtime. Use this method to add services to the container.

    public void RegistreServices(IServiceCollection services)
    {
      DataContext _context;
      var conn = XmlConnection.ReadVariablesSystem();

      _context = new DataContext(conn.Server, conn.DataBase);
      services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
      IServiceAuthentication serviceAuthentication = new ServiceAuthentication(_context);
      IServiceAccountUser serviceAccount = new ServiceAccountUser(_context);
      IServiceCharacter serviceCharacter = new ServiceCharacter(_context);
      IServiceGame serviceGame = new ServiceGame(_context);
      IServiceLog serviceLog = new ServiceLog(_context);
      IServiceMission serviceMission = new ServiceMission(_context);
      IServicePlanet servicePlanet = new ServicePlanet(_context);

      services.AddSingleton(_ => serviceAuthentication);
      services.AddSingleton(_ => serviceAccount);
      services.AddSingleton(_ => serviceCharacter);
      services.AddSingleton(_ => serviceGame);
      services.AddSingleton(_ => serviceLog);
      services.AddSingleton(_ => serviceMission);
      services.AddSingleton(_ => servicePlanet);

    }

    public void ConfigureServices(IServiceCollection services)
    {
      services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
      .AddJwtBearer(options =>
      {
        options.TokenValidationParameters = new TokenValidationParameters
        {
          ValidateIssuer = false,
          ValidateAudience = false,
          ValidateLifetime = true,
          ValidateIssuerSigningKey = true,
          IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secret))
        };
        options.Events = new JwtBearerEvents
        {
          OnAuthenticationFailed = context =>
          {
            Console.WriteLine("OnAuthenticationFailed: " + context.Exception.Message);
            return Task.CompletedTask;
          },
          OnTokenValidated = context =>
          {
            Console.WriteLine("OnTokenValidated: " + context.SecurityToken);
            return Task.CompletedTask;
          }
        };
      });
      services.AddCors(options =>
        options.AddPolicy("AllowAll",
          builder => builder
          .AllowAnyOrigin()
          .AllowAnyMethod()
          .AllowAnyHeader()
          .AllowCredentials()
      ));

      services.AddMvc();
      RegistreServices(services);

    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
        app.UseDeveloperExceptionPage();
      app.UseAuthentication();
      app.UseCors("AllowAll");
      app.UseMvc();
    }
  }
}
