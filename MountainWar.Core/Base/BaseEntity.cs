﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MountainWar.Core.Enumns;

namespace MountainWar.Core.Base
{
  public class BaseEntity
  {
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string _id { get; set; }
    public EnumStatus Status { get; set; }
  }
}
