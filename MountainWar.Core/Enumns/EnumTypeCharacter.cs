﻿namespace MountainWar.Core.Enumns
{
  public enum EnumTypeCharacter : byte
  {
    Hybrid = 0, Warrior = 1, Magic = 2, Archer = 3, Kruts = 4,
    MagicMistis = 5, GeneralScar = 6, OldMan = 7, Snake = 8, WaterSnake = 9,
    Dragon = 10, Ghost = 11, Undead = 12, IceGiant = 13, KingScar = 14, Skeleton = 15
  }
}
