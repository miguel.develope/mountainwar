﻿namespace MountainWar.Core.Enumns
{
  public enum EnumLevel : byte
  {
    Easy = 0, Medium = 1, Hard = 2
  }
}
