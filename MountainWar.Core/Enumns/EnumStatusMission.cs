﻿namespace MountainWar.Core.Enumns
{
  public enum EnumStatusMission : byte
  {
    Open = 0, Wait = 1, End = 2
  }
}
