﻿namespace MountainWar.Core.Enumns
{
  public enum EnumTypeUser : byte
  {
    Default = 0, Administrator = 1, God = 2, Anonymous = 3
  }
}
