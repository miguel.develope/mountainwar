﻿namespace MountainWar.Core.Enumns
{
  public enum EnumItens : byte
  {
    Stone = 0, StoneDesires = 1, Sword = 2, Archery = 3, Dagger = 4, Ring = 5, Book = 6, Cloak = 7, Armor = 8, Glasses = 9, Shield = 10, Spear = 11
  }
}
