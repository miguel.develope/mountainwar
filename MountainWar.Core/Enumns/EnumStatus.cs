﻿namespace MountainWar.Core.Enumns
{
  public enum EnumStatus : byte
  {
    Enabled = 0, Disabled = 1, Excluded = 2
  }
}
