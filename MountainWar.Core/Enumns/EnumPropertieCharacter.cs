﻿namespace MountainWar.Core.Enumns
{
  public enum EnumPropertieCharacter : byte
  {
    Life = 0, Power = 1, Mana = 2, Money = 3, Resistance = 4
  }
}
