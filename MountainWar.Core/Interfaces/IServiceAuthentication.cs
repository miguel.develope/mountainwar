﻿using MountainWar.Core.Bussiness;
using MountainWar.Core.Views;

namespace MountainWar.Core.Interfaces
{
  public interface IServiceAuthentication
  {
    ViewUser Authentication(string mail, string password);
    string Logoff(string id);
    void LogSave(AccountUser user);
  }
}
