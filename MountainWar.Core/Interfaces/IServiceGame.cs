﻿using MountainWar.Core.Views;
using System.Collections.Generic;

namespace MountainWar.Core.Interfaces
{
  public interface IServiceGame
  {
    string NewGame(ViewNewGame view);
    string UpdateGame(ViewNewGame view, string id);
    string SaveGame(ViewNewGame view, string id);
    string EndMissionGame(ViewNewGame view, string id);
    string UpdatePercent(ViewNewGame view, string id);
    string GameOver(string id);
    List<ViewGame> ListGames(string iduser);
  }
}
