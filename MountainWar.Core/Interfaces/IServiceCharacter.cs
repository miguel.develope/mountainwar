﻿using MountainWar.Core.Enumns;
using MountainWar.Core.Views;
using System.Collections.Generic;

namespace MountainWar.Core.Interfaces
{
  public interface IServiceCharacter
  {
    string NewCharacter(ViewCreateCharacter view, string id);
    string UpdateCharacter(ViewCreateCharacter view, string iduser, string idcharacter);
    decimal UpLife(decimal value, string idcharacter, string iduser);
    decimal UpResistance(decimal value, string idcharacter, string iduser);
    decimal UpMana(decimal value, string idcharacter, string iduser);
    decimal UpPower(decimal value, string idcharacter, string iduser);
    decimal UpMoney(decimal value, string idcharacter, string iduser);
    decimal DownLife(decimal value, string idcharacter, string iduser);
    decimal DownResistance(decimal value, string idcharacter, string iduser);
    decimal DownMana(decimal value, string idcharacter, string iduser);
    decimal DownPower(decimal value, string idcharacter, string iduser);
    decimal DownMoney(decimal value, string idcharacter, string iduser);
    int UpdateNumberStrikes(string idcharacter, string iduser);
    string AddItens(EnumItens item, string idcharacter, string iduser);
    string AddSpells(EnumSpells spell, string idcharacter, string iduser);
    string DeadCharacter(string iduser);
    ViewCharacter GetCharacter(string iduser);
    List<ViewCharacter> ListCharacters();
    string Price(EnumPropertieCharacter type, string iduser, string idcharacter);
  }
}
