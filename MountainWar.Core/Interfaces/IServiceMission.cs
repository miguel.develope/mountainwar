﻿using MountainWar.Core.Bussiness;
using MountainWar.Core.Views;
using System.Collections.Generic;

namespace MountainWar.Core.Interfaces
{
  public interface IServiceMission
  {
    string NewMission(ViewMissionCreate view);
    string UpdateMission(ViewMissionCreate view, int number);
    List<ViewListMission> GetMissions();
    ViewListMission GetMission(string id);
    string GetMessageDialog(int mission, int dialog);
    List<ViewListCharacter> GetCharacteres(int mission);
    string UpdateMissionSize(ViewMissionCreate view, int number);
  }
}
