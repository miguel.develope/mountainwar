﻿using MountainWar.Core.Bussiness;
using MountainWar.Core.Enumns;
using MountainWar.Core.Views;
using System.Collections.Generic;

namespace MountainWar.Core.Interfaces
{
  public interface IServicePlanet
  {
    string NewPlanet(ViewCreatePlanet view);
    List<Planet> ListPlanets();
    string UpdatePlanet(ViewCreatePlanet view, EnumTypePlanet type);
  }
}
