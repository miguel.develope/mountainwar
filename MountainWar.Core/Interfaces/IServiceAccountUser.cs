﻿using MountainWar.Core.Bussiness;
using MountainWar.Core.Views;

namespace MountainWar.Core.Interfaces
{
  public interface IServiceAccountUser
  {
    string NewAccount(ViewCreateAccount view);
    string AlterAccount(ViewCreateAccount view, string id);
    string AlterPassword(ViewCreateAccount view, string id);
    string AddCharacter(Character character, string id);
  }
}
