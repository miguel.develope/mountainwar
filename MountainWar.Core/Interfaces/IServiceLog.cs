﻿using MountainWar.Core.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace MountainWar.Core.Interfaces
{
  public interface IServiceLog
  {
    void NewLog(ViewLog view);
  }
}
