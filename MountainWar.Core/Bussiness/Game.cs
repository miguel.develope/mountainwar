﻿using MongoDB.Bson;
using MountainWar.Core.Base;
using MountainWar.Core.Enumns;
using MountainWar.Core.Views;
using System;
using System.Collections.Generic;

namespace MountainWar.Core.Bussiness
{
  public class Game : BaseEntity
  {
    public string Name { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime? EndDate { get; set; }
    public ViewListAccountUser Account { get; set; }
    public ViewListMission EndMission { get; set; }
    public List<ViewListMission> Missions { get; set; }
    public decimal Percent { get; set; }
    public EnumLevel Level { get; set; }
    public decimal Latitude { get; set; }
    public decimal Longitude { get; set; }
  }
}
