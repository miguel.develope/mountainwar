﻿using MountainWar.Core.Base;
using MountainWar.Core.Enumns;
using MountainWar.Core.Views;

namespace MountainWar.Core.Bussiness
{
  public class Planet : BaseEntity
  {
    public string Name { get; set; }
    public decimal Width { get; set; }
    public decimal Height { get; set; }
    public EnumTypePlanet TypePlanet { get; set; }
    public ViewListPlanet GetViewList()
    {
      return new ViewListPlanet()
      {
        _id = _id,
        Name = Name,
        TypePlanet = TypePlanet
      };
    }
  }
}
