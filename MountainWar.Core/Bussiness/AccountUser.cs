﻿using MountainWar.Core.Base;
using MountainWar.Core.Enumns;
using MountainWar.Core.Views;
using System;

namespace MountainWar.Core.Bussiness
{
  public class AccountUser : BaseEntity
  {
    public string Name { get; set; }
    public string Mail { get; set; }
    public string Password { get; set; }
    public EnumTypeUser Type { get; set; }
    public ViewListCharacter Character { get; set; }
    public DateTime? BirthDay { get; set; }
    public DateTime StartAccount { get; set; }
    public string Photo { get; set; }
    public EnumChangePassword ChangePassword { get; set; }
    public ViewListAccountUser GetListView()
    {
      return new ViewListAccountUser()
      {
        _id = _id,
        Name = Name,
        Photo = Photo
      };
    }
  }
}
