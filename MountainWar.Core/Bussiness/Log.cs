﻿using MongoDB.Bson;
using MountainWar.Core.Base;
using MountainWar.Core.Views;
using System;

namespace MountainWar.Core.Bussiness
{
  public class Log : BaseEntity
  {
    public string Description { get; set; }
    public ViewListAccountUser AccountUser { get; set; }
    public DateTime DateLog { get; set; }
    public string Local { get; set; }
  }
}
