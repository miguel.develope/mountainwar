﻿using MountainWar.Core.Base;
using MountainWar.Core.Enumns;
using MountainWar.Core.Views;
using System;
using System.Collections.Generic;

namespace MountainWar.Core.Bussiness
{
  public class Mission : BaseEntity
  {
    public string Name { get; set; }
    public int Number { get; set; }
    public EnumStatusMission StatusMission { get; set; }
    public DateTime? StartDate { get; set; }
    public DateTime? EndDate { get; set; }
    public ViewListPlanet Planet { get; set; }
    public decimal StartLatitude { get; set; }
    public decimal EndLatitude { get; set; }
    public decimal StartLongitude { get; set; }
    public decimal EndLongitude { get; set; }
    public List<ViewListDialog> Dialog { get; set; }
    public List<ViewListCharacter> Character { get; set; }
    public EnumLevel Level { get; set; }
    public ViewListMission GetListView()
    {
      return new ViewListMission()
      {
        _id = _id,
        Name = Name,
        Number = Number,
        StatusMission = StatusMission,
        StartDate = StartDate,
        EndDate = EndDate,
        Planet = Planet,
        StartLatitude = StartLatitude,
        EndLatitude = EndLatitude,
        StartLongitude = StartLongitude,
        EndLongitude = EndLongitude,
        Dialog = Dialog,
        Character = Character,
        Level = Level
      };
    }
  }
}
