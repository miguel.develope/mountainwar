﻿using MountainWar.Core.Base;
using MountainWar.Core.Views;

namespace MountainWar.Core.Bussiness
{
  public class Dialog : BaseEntity
  {
    public int Number { get; set; }
    public string Description { get; set; }
    public ViewListDialog GetListView()
    {
      return new ViewListDialog()
      {
        Number = Number,
        Description = Description
      };
    }
  }
}
