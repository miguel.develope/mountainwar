﻿using MountainWar.Core.Base;
using MountainWar.Core.Enumns;
using MountainWar.Core.Views;
using System.Collections.Generic;

namespace MountainWar.Core.Bussiness
{
  public class Character : BaseEntity
  {
    public string Name { get; set; }
    public string Color { get; set; }
    public decimal Life { get; set; }
    public decimal Resistance { get; set; }
    public decimal Mana { get; set; }
    public decimal Power { get; set; }
    public decimal Money { get; set; }
    public EnumTypeCharacter Type { get; set; }
    public List<EnumItens> Itens { get; set; }
    public List<EnumSpells> Spells { get; set; }
    public int NumberStrikes { get; set; }
    public ViewListCharacter GetListView()
    {
      return new ViewListCharacter()
      {
        _id = _id,
        Name = Name,
        Color = Color,
        Life = Life,
        Resistance = Resistance,
        Mana = Mana,
        Power = Power,
        Money = Money,
        Type = Type,
        Itens = Itens,
        Spells = Spells,
        NumberStrikes = NumberStrikes
      };
    }
  }
}
