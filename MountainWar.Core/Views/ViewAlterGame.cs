﻿using MountainWar.Core.Enumns;
using System;

namespace MountainWar.Core.Views
{
  public class ViewAlterGame
  {
    public string Name { get; set; }
    public DateTime? EndDate { get; set; }
    public int EndMission { get; set; }
    public EnumLevel Level { get; set; }
    public decimal Latitude { get; set; }
    public decimal Longitude { get; set; }
  }
}
