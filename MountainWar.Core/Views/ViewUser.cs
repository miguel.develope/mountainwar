﻿using MountainWar.Core.Enumns;

namespace MountainWar.Core.Views
{
  public class ViewUser
  {
    public string IdUser { get; set; }
    public string Name { get; set; }
    public string Mail { get; set; }
    public string Token { get; set; }
    public string Photo { get; set; }
    public EnumChangePassword ChangePassword { get; set; }
    public EnumTypeUser TypeUser { get; set; }
  }
}
