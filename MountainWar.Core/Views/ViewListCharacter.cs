﻿using MountainWar.Core.Enumns;
using System.Collections.Generic;

namespace MountainWar.Core.Views
{
  public class ViewListCharacter: _ViewListBase
  {
    public string Color { get; set; }
    public decimal Life { get; set; }
    public decimal Resistance { get; set; }
    public decimal Mana { get; set; }
    public decimal Power { get; set; }
    public decimal Money { get; set; }
    public EnumTypeCharacter Type { get; set; }
    public List<EnumItens> Itens { get; set; }
    public List<EnumSpells> Spells { get; set; }
    public int NumberStrikes { get; set; }
  }
}
