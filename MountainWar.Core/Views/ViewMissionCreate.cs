﻿using MountainWar.Core.Bussiness;
using MountainWar.Core.Enumns;
using System;
using System.Collections.Generic;
using System.Text;

namespace MountainWar.Core.Views
{
  public class ViewMissionCreate
  {
    public string Name { get; set; }
    public int Number { get; set; }
    public EnumTypePlanet Planet { get; set; }
    public decimal StartLatitude { get; set; }
    public decimal EndLatitude { get; set; }
    public decimal StartLongitude { get; set; }
    public decimal EndLongitude { get; set; }
    public List<ViewListDialog> Dialog { get; set; }
    public List<ViewListCharacter> Character { get; set; }
    public EnumLevel Level { get; set; }
  }
}
