﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MountainWar.Core.Views
{
  public class ViewCreateAccount
  {
    public string Name { get; set; }
    public string Mail { get; set; }
    public string Password { get; set; }
    public DateTime? BirthDay { get; set; }
  }
}
