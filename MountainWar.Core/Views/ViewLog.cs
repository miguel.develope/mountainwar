﻿
using MountainWar.Core.Bussiness;

namespace MountainWar.Core.Views
{
  public class ViewLog
  {
    public ViewListAccountUser Account { get; set; }
    public string Local { get; set; }
    public string Description { get; set; }
  }
}
