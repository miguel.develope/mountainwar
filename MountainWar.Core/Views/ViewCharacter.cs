﻿using MongoDB.Bson;
using MountainWar.Core.Enumns;
using System;
using System.Collections.Generic;
using System.Text;

namespace MountainWar.Core.Views
{
  public class ViewCharacter
  {
    public string _id { get; set; }
    public string Name { get; set; }
    public string Color { get; set; }
    public decimal Life { get; set; }
    public decimal Resistance { get; set; }
    public decimal Mana { get; set; }
    public decimal Power { get; set; }
    public decimal Money { get; set; }
    public EnumTypeCharacter Type { get; set; }
    public List<EnumItens> Itens { get; set; }
    public List<EnumSpells> Spells { get; set; }
    public int NumberStrikes { get; set; }
  }
}
