﻿using MountainWar.Core.Enumns;
using System;

namespace MountainWar.Core.Views
{
  public class ViewNewGame
  {
    public string IdAccount { get; set; }
    public EnumLevel Level { get; set; }
    public int Mission { get; set; }
  }
}
