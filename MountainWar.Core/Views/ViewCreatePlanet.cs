﻿using MountainWar.Core.Enumns;

namespace MountainWar.Core.Views
{
  public class ViewCreatePlanet
  {
    public string Name { get; set; }
    public decimal Width { get; set; }
    public decimal Height { get; set; }
    public EnumTypePlanet TypePlanet { get; set; }
  }
}
