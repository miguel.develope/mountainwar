﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MountainWar.Core.Views
{
  public class ViewListDialog
  {
    public int Number { get; set; }
    public string Description { get; set; }
  }
}
