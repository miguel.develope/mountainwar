﻿using MongoDB.Bson;
using MountainWar.Core.Base;
using MountainWar.Core.Enumns;

namespace MountainWar.Core.Views
{
  public class ViewGame
  {
    public string Name { get; set; }
    public string Id { get; set; }
    public string EndMission { get; set; }
    public decimal Percent { get; set; }
    public EnumLevel Level { get; set; }
    public decimal Latitude { get; set; }
    public decimal Longitude { get; set; }
  }
}
