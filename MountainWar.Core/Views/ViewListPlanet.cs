﻿using MountainWar.Core.Enumns;

namespace MountainWar.Core.Views
{
  public class ViewListPlanet: _ViewListBase
  {
    public EnumTypePlanet TypePlanet { get; set; }
  }
}
