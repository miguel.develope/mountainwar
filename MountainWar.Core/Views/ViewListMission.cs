﻿using MountainWar.Core.Enumns;
using System;
using System.Collections.Generic;
using System.Text;

namespace MountainWar.Core.Views
{
  public class ViewListMission: _ViewListBase
  {
    public int Number { get; set; }
    public EnumStatusMission StatusMission { get; set; }
    public DateTime? StartDate { get; set; }
    public DateTime? EndDate { get; set; }
    public ViewListPlanet Planet { get; set; }
    public decimal StartLatitude { get; set; }
    public decimal EndLatitude { get; set; }
    public decimal StartLongitude { get; set; }
    public decimal EndLongitude { get; set; }
    public List<ViewListDialog> Dialog { get; set; }
    public EnumLevel Level { get; set; }

    public List<ViewListCharacter> Character { get; set; }
  }
}
