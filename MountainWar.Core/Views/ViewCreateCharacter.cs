﻿using MountainWar.Core.Enumns;

namespace MountainWar.Core.Views
{
  public class ViewCreateCharacter
  {
    public string Color { get; set; }
    public EnumTypeCharacter Type { get; set; }
  }
}
