﻿using MongoDB.Bson;
using MountainWar.Core.Bussiness;
using MountainWar.Core.Enumns;
using MountainWar.Core.Interfaces;
using MountainWar.Core.Views;
using MountainWar.Data;
using MountainWar.Service.Commons;
using System;
using System.Collections.Generic;
using System.Text;

namespace MountainWar.Service.Specific
{
  public class ServiceLog : Repository<Log>, IServiceLog
  {
    private readonly ServiceGeneric<Log> service;

    public ServiceLog(DataContext context)
      : base(context)
    {
      service = new ServiceGeneric<Log>(context);
    }

    public void NewLog(ViewLog view)
    {
      try
      {
        var log = new Log
        {
          AccountUser = view.Account,
          DateLog = DateTime.Now,
          Description = view.Description,
          Status = EnumStatus.Enabled,
          Local = view.Local
        };
        service.Insert(log);
      }
      catch (Exception e)
      {
        throw e;
      }
    }
  }
}
