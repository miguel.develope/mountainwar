﻿using MongoDB.Bson;
using MountainWar.Core.Bussiness;
using MountainWar.Core.Enumns;
using MountainWar.Core.Interfaces;
using MountainWar.Core.Views;
using MountainWar.Data;
using MountainWar.Service.Commons;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MountainWar.Service.Specific
{
  public class ServiceMission : Repository<Mission>, IServiceMission
  {
    private readonly ServiceGeneric<Mission> service;
    private readonly ServiceGeneric<Planet> servicePlanet;

    public ServiceMission(DataContext context) : base(context)
    {
      service = new ServiceGeneric<Mission>(context);
      servicePlanet = new ServiceGeneric<Planet>(context);
    }

    public ViewListMission GetMission(string id)
    {
      return service.GetAll(p => p._id == id).FirstOrDefault().GetListView();
    }

    public List<ViewListMission> GetMissions()
    {
      return service.GetAll(p => p.Status == EnumStatus.Enabled).
        Select(p => new ViewListMission()
        {
          _id = p._id,
          Name = p.Name,
          Number = p.Number,
          StatusMission = p.StatusMission,
          StartDate = p.StartDate,
          EndDate = p.EndDate,
          Planet = p.Planet,
          StartLatitude = p.StartLatitude,
          EndLatitude = p.EndLatitude,
          StartLongitude = p.StartLongitude,
          EndLongitude = p.EndLongitude,
          Dialog = p.Dialog,
          Character = p.Character,
          Level = p.Level
        }).ToList();
    }

    public string GetMessageDialog(int mission, int dialog)
    {

      return service.GetAll(p => p.Number == mission).FirstOrDefault().Dialog.Where(p => p.Number == dialog).FirstOrDefault().Description;
    }

    public List<ViewListCharacter> GetCharacteres(int mission)
    {

      return service.GetAll(p => p.Number == mission).FirstOrDefault().Character;
    }

    public string NewMission(ViewMissionCreate view)
    {
      var mission = new Mission()
      {
        Name = view.Name,
        Number = view.Number,
        StartLatitude = view.StartLatitude,
        StartLongitude = view.StartLongitude,
        EndLatitude = view.EndLatitude,
        EndLongitude = view.EndLongitude,
        Planet = servicePlanet.GetAll(p => p.TypePlanet == view.Planet).FirstOrDefault().GetViewList(),
        StatusMission = EnumStatusMission.Open,
        Status = EnumStatus.Enabled,
        Dialog = view.Dialog,
        Level = view.Level,
        Character = view.Character
      };

      service.Insert(mission);

      return "new mission";
    }

    public string UpdateMissionSize(ViewMissionCreate view, int number)
    {
      var mission = service.GetAll(p => p.Number == number).FirstOrDefault();

      mission.StartLatitude = view.StartLatitude;
      mission.StartLongitude = view.StartLongitude;
      mission.EndLatitude = view.EndLatitude;
      mission.EndLongitude = view.EndLongitude;

      service.Update(mission, null);

      return "new mission";
    }

    public string UpdateMission(ViewMissionCreate view, int number)
    {
      var mission = service.GetAll(p => p.Number == number).FirstOrDefault();
      mission.Character = view.Character;
      mission.Dialog = view.Dialog;
      service.Update(mission, null);
      return "update mission";
    }
  }
}
