﻿using MongoDB.Bson;
using MountainWar.Core.Bussiness;
using MountainWar.Core.Enumns;
using MountainWar.Core.Interfaces;
using MountainWar.Core.Views;
using MountainWar.Data;
using MountainWar.Service.Commons;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MountainWar.Service.Specific
{
  public class ServiceGame : Repository<Game>, IServiceGame
  {
    private readonly ServiceGeneric<Game> service;
    private readonly ServiceGeneric<AccountUser> serviceAccountUser;
    private readonly ServiceGeneric<Character> serviceCharacter;
    private readonly ServiceGeneric<Mission> serviceMission;

    public ServiceGame(DataContext context) : base(context)
    {
      service = new ServiceGeneric<Game>(context);
      serviceAccountUser = new ServiceGeneric<AccountUser>(context);
      serviceCharacter = new ServiceGeneric<Character>(context);
      serviceMission = new ServiceGeneric<Mission>(context);

    }

    public string GameOver(string id)
    {
      var game = service.GetAll(p => p._id == id).FirstOrDefault();
      game.Status = EnumStatus.Disabled;
      service.Update(game, null);
      return "game over";
    }

    public List<ViewGame> ListGames(string iduser)
    {
      return (from game in service.GetAll()
              where game.Account._id == iduser
              select game).ToList().Select(
              game => new ViewGame()
              {
                Id = game._id,
                Name = game.Name,
                Level = game.Level,
                Latitude = game.Latitude,
                Longitude = game.Longitude,
                EndMission = game.EndMission.Name,
                Percent = game.Percent
              }
             ).ToList();
    }

    public string NewGame(ViewNewGame view)
    {
      var game = new Game()
      {
        Account = serviceAccountUser.GetAll(p => p._id == view.IdAccount).FirstOrDefault().GetListView(),
        Level = view.Level,
        StartDate = DateTime.Now,
        Latitude = 0,
        Longitude = 0,
        Percent = 0,
        Status = EnumStatus.Enabled
      };
      game.Name = serviceAccountUser.GetAll(p => p._id == view.IdAccount).FirstOrDefault().Name + " - " + DateTime.Now;

      var mission = serviceMission.GetAll(p => p.Number == 1 & p.Level == game.Level).FirstOrDefault();
      mission.StartDate = DateTime.Now;
      mission.StatusMission = EnumStatusMission.Wait;
      game.EndMission = mission.GetListView();

      var listmissions = serviceMission.GetAll(p => p.Status == EnumStatus.Enabled & p.Level == game.Level)
        .Select(p => new ViewListMission()
        {
          _id = p._id,
          Name = p.Name,
          Number = p.Number,
          StatusMission = p.StatusMission,
          StartDate = p.StartDate,
          EndDate = p.EndDate,
          Planet = p.Planet,
          StartLatitude = p.StartLatitude,
          EndLatitude = p.EndLatitude,
          StartLongitude = p.StartLongitude,
          EndLongitude = p.EndLongitude,
          Dialog = p.Dialog,
          Character = p.Character,
          Level = p.Level
        })
        .ToList();
      game.Missions = listmissions;
      service.Insert(game);


      return "new game";
    }

    public string UpdateGame(ViewNewGame view, string id)
    {
      var game = service.GetAll(p => p._id == id).FirstOrDefault();
      game.Level = view.Level;
      service.Update(game, null);
      return "update game";
    }

    public string SaveGame(ViewNewGame view, string id)
    {
      var game = service.GetAll(p => p._id == id).FirstOrDefault();
      game.Name = game.Account.Name + " - " + DateTime.Now;
      var mission = serviceMission.GetAll(p => p.Number == view.Mission & p.Level == game.Level).FirstOrDefault();
      mission.EndDate = DateTime.Now;
      game.EndMission = mission.GetListView();

      service.Update(game, null);
      return "save game";
    }

    public string EndMissionGame(ViewNewGame view, string id)
    {
      var game = service.GetAll(p => p._id == id).FirstOrDefault();
      game.Name = game.Account.Name + " - " + DateTime.Now;
      //next mission
      var mission = serviceMission.GetAll(p => p.Number == (view.Mission + 1) & p.Level == game.Level).FirstOrDefault();
      mission.StatusMission = EnumStatusMission.Wait;
      mission.StartDate = DateTime.Now;
      var list = new List<ViewListMission>();
      foreach (var item in game.Missions)
      {
        if (item.Number == view.Mission)
        {
          item.EndDate = DateTime.Now;
          item.StatusMission = EnumStatusMission.End;
        }
        list.Add(item);
      }
      game.Missions = list;
      game.EndMission = mission.GetListView();

      service.Update(game, null);
      return "save game";
    }

    public string UpdatePercent(ViewNewGame view, string id)
    {
      var game = service.GetAll(p => p._id == id).FirstOrDefault();
      game.Percent = 0;
      service.Update(game, null);
      return "update percent";
    }
  }
}
