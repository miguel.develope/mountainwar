﻿using System.Collections.Generic;
using MountainWar.Core.Bussiness;
using MountainWar.Core.Interfaces;
using MountainWar.Core.Views;
using MountainWar.Data;
using MountainWar.Service.Commons;
using System.Linq;
using System;
using MountainWar.Core.Enumns;

namespace MountainWar.Service.Specific
{
  public class ServicePlanet : Repository<Planet>, IServicePlanet
  {
    private readonly ServiceGeneric<Planet> service;

    public ServicePlanet(DataContext context) : base(context)
    {
      service = new ServiceGeneric<Planet>(context);
    }

    public List<Planet> ListPlanets()
    {
      try
      {
        return service.GetAll(p => p.Status == EnumStatus.Enabled).ToList();
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public string NewPlanet(ViewCreatePlanet view)
    {
      try
      {
        var planet = new Planet()
        {
          Name = view.Name,
          Height = view.Height,
          Width = view.Width,
          TypePlanet = view.TypePlanet,
          Status = EnumStatus.Enabled,
        };
        service.Insert(planet);
        return "ok";
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public string UpdatePlanet(ViewCreatePlanet view, EnumTypePlanet type)
    {
      try
      {
        var planet = service.GetAll(p => p.TypePlanet == type).FirstOrDefault();
        planet.Height = view.Height;
        planet.Width = view.Width;
        service.Update(planet, null);
        return "ok";
      }
      catch (Exception e)
      {
        throw e;
      }
    }
  }
}
