﻿using MongoDB.Bson;
using MountainWar.Core.Bussiness;
using MountainWar.Core.Enumns;
using MountainWar.Core.Interfaces;
using MountainWar.Core.Views;
using MountainWar.Data;
using MountainWar.Service.Auth;
using MountainWar.Service.Commons;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MountainWar.Service.Specific
{
  public class ServiceCharacter : Repository<Character>, IServiceCharacter
  {
    private readonly ServiceGeneric<Character> service;
    private readonly ServiceAccountUser serviceAccountUser;

    public ServiceCharacter(DataContext context) : base(context)
    {
      service = new ServiceGeneric<Character>(context);
      serviceAccountUser = new ServiceAccountUser(context);
    }

    public string AddItens(EnumItens item, string idcharacter, string iduser)
    {
      var character = service.GetAll(p => p._id == idcharacter).FirstOrDefault();
      character.Itens.Add(item);
      service.Update(character, null);
      var user = serviceAccountUser.GetAll(p => p._id == iduser).FirstOrDefault();
      user.Character = character.GetListView();
      serviceAccountUser.Update(user, null);

      return "new item add";
    }

    public string AddSpells(EnumSpells spell, string idcharacter, string iduser)
    {
      var character = service.GetAll(p => p._id == idcharacter).FirstOrDefault();
      character.Spells.Add(spell);
      service.Update(character, null);
      var user = serviceAccountUser.GetAll(p => p._id == iduser).FirstOrDefault();
      user.Character = character.GetListView();
      serviceAccountUser.Update(user, null);

      return "new item add";
    }

    public string DeadCharacter(string iduser)
    {
      var user = serviceAccountUser.GetAll(p => p._id == iduser).FirstOrDefault();
      var character = service.GetAll(p => p._id == user.Character._id).FirstOrDefault();
      character.Status = EnumStatus.Excluded;
      service.Update(character, null);

      user.Character = null;
      serviceAccountUser.Update(user, null);
      return "dead";
    }

    public ViewCharacter GetCharacter(string iduser)
    {
      return (from character in service.GetAll()
              where character._id == iduser
              select character).ToList().
              Select(character => new ViewCharacter()
              {
                Name = character.Name,
                _id = character._id,
                Color = character.Color,
                Itens = character.Itens,
                Life = character.Life,
                Mana = character.Mana,
                Money = character.Money,
                NumberStrikes = character.NumberStrikes,
                Power = character.Power,
                Resistance = character.Resistance,
                Spells = character.Spells,
                Type = character.Type
              }).FirstOrDefault();
    }

    public List<ViewCharacter> ListCharacters()
    {
      return (from character in service.GetAll()
              where character.Status == EnumStatus.Enabled
              select character).ToList().
              Select(character => new ViewCharacter()
              {
                Name = character.Name,
                _id = character._id,
                Color = character.Color,
                Itens = character.Itens,
                Life = character.Life,
                Mana = character.Mana,
                Money = character.Money,
                NumberStrikes = character.NumberStrikes,
                Power = character.Power,
                Resistance = character.Resistance,
                Spells = character.Spells,
                Type = character.Type
              }).ToList();
    }

    public string NewCharacter(ViewCreateCharacter view, string id)
    {
      var user = serviceAccountUser.GetAll(p => p._id == id).FirstOrDefault();
      var character = new Character()
      {
        Name = user.Name,
        Color = view.Color,
        Type = view.Type,
        Itens = new List<EnumItens>(),
        Spells = new List<EnumSpells>(),
        Life = 100,
        Mana = 10,
        Money = 10,
        NumberStrikes = 0,
        Power = 10,
        Resistance = 10,
        Status = EnumStatus.Enabled
      };
      service.Insert(character);

      user.Character = character.GetListView();
      serviceAccountUser.Update(user, null);

      return "create";
    }

    public string UpdateCharacter(ViewCreateCharacter view, string iduser, string idcharacter)
    {
      var character = service.GetAll(p => p._id == idcharacter).FirstOrDefault();
      character.Color = view.Color;
      character.Type = view.Type;

      service.Update(character, null);

      var user = serviceAccountUser.GetAll(p => p._id == iduser).FirstOrDefault();
      user.Character = character.GetListView();
      serviceAccountUser.Update(user, null);

      return "update character";
    }


    public string Price(EnumPropertieCharacter type, string iduser, string idcharacter)
    {
      try
      {
        switch (type)
        {
          case EnumPropertieCharacter.Life:
            DownMoney(1500, idcharacter, iduser);
            UpLife(1, idcharacter, iduser);
            break;
          case EnumPropertieCharacter.Power:
            DownMoney(1200, idcharacter, iduser);
            UpPower(1, idcharacter, iduser);
            break;
          case EnumPropertieCharacter.Resistance:
            DownMoney(100, idcharacter, iduser);
            UpResistance(1, idcharacter, iduser);
            break;
          case EnumPropertieCharacter.Money:
            DownMoney(1, idcharacter, iduser);
            UpMoney(1, idcharacter, iduser);
            break;
          case EnumPropertieCharacter.Mana:
            DownMoney(1200, idcharacter, iduser);
            UpMana(1, idcharacter, iduser);
            break;
          default:
            DownMoney(0, idcharacter, iduser);
            break;
        }
        return "sale";
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public decimal UpLife(decimal quantity, string idcharacter, string iduser)
    {
      var character = service.GetAll(p => p._id == idcharacter).FirstOrDefault();
      character.Life += quantity;
      service.Update(character, null);
      var user = serviceAccountUser.GetAll(p => p._id == iduser).FirstOrDefault();
      user.Character = character.GetListView();
      serviceAccountUser.Update(user, null);

      return character.Life;
    }

    public decimal UpMana(decimal quantity, string idcharacter, string iduser)
    {
      var character = service.GetAll(p => p._id == idcharacter).FirstOrDefault();
      character.Mana += quantity;
      service.Update(character, null);
      var user = serviceAccountUser.GetAll(p => p._id == iduser).FirstOrDefault();
      user.Character = character.GetListView();
      serviceAccountUser.Update(user, null);

      return character.Mana;
    }

    public decimal UpMoney(decimal quantity, string idcharacter, string iduser)
    {
      var character = service.GetAll(p => p._id == idcharacter).FirstOrDefault();
      character.Money += quantity;
      service.Update(character, null);
      var user = serviceAccountUser.GetAll(p => p._id == iduser).FirstOrDefault();
      user.Character = character.GetListView();
      serviceAccountUser.Update(user, null);

      return character.Money;
    }

    public decimal UpPower(decimal quantity, string idcharacter, string iduser)
    {
      var character = service.GetAll(p => p._id == idcharacter).FirstOrDefault();
      character.Power += quantity;
      service.Update(character, null);
      var user = serviceAccountUser.GetAll(p => p._id == iduser).FirstOrDefault();
      user.Character = character.GetListView();
      serviceAccountUser.Update(user, null);

      return character.Power;
    }

    public decimal UpResistance(decimal quantity, string idcharacter, string iduser)
    {
      var character = service.GetAll(p => p._id == idcharacter).FirstOrDefault();
      character.Resistance += quantity;
      service.Update(character, null);
      var user = serviceAccountUser.GetAll(p => p._id == iduser).FirstOrDefault();
      user.Character = character.GetListView();
      serviceAccountUser.Update(user, null);

      return character.Resistance;
    }

    public decimal DownLife(decimal quantity, string idcharacter, string iduser)
    {
      var character = service.GetAll(p => p._id == idcharacter).FirstOrDefault();
      character.Life -= quantity;
      service.Update(character, null);
      var user = serviceAccountUser.GetAll(p => p._id == iduser).FirstOrDefault();
      user.Character = character.GetListView();
      serviceAccountUser.Update(user, null);

      return character.Life;
    }

    public decimal DownMana(decimal quantity, string idcharacter, string iduser)
    {
      var character = service.GetAll(p => p._id == idcharacter).FirstOrDefault();
      character.Mana -= quantity;
      service.Update(character, null);
      var user = serviceAccountUser.GetAll(p => p._id == iduser).FirstOrDefault();
      user.Character = character.GetListView();
      serviceAccountUser.Update(user, null);

      return character.Mana;
    }

    public decimal DownMoney(decimal quantity, string idcharacter, string iduser)
    {
      var character = service.GetAll(p => p._id == idcharacter).FirstOrDefault();
      character.Money -= quantity;
      service.Update(character, null);
      var user = serviceAccountUser.GetAll(p => p._id == iduser).FirstOrDefault();
      user.Character = character.GetListView();
      serviceAccountUser.Update(user, null);

      return character.Money;
    }

    public decimal DownPower(decimal quantity, string idcharacter, string iduser)
    {
      var character = service.GetAll(p => p._id == idcharacter).FirstOrDefault();
      character.Power -= quantity;
      service.Update(character, null);
      var user = serviceAccountUser.GetAll(p => p._id == iduser).FirstOrDefault();
      user.Character = character.GetListView();
      serviceAccountUser.Update(user, null);

      return character.Power;
    }

    public decimal DownResistance(decimal quantity, string idcharacter, string iduser)
    {
      var character = service.GetAll(p => p._id == idcharacter).FirstOrDefault();
      character.Resistance -= quantity;
      service.Update(character, null);
      var user = serviceAccountUser.GetAll(p => p._id == iduser).FirstOrDefault();
      user.Character = character.GetListView();
      serviceAccountUser.Update(user, null);

      return character.Resistance;
    }

    public int UpdateNumberStrikes(string idcharacter, string iduser)
    {
      var character = service.GetAll(p => p._id == idcharacter).FirstOrDefault();
      character.NumberStrikes += 1;
      service.Update(character, null);
      var user = serviceAccountUser.GetAll(p => p._id == iduser).FirstOrDefault();
      user.Character = character.GetListView();
      serviceAccountUser.Update(user, null);

      return character.NumberStrikes;
    }
  }
}
