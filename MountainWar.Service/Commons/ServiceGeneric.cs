﻿using MountainWar.Core.Base;
using MountainWar.Data;

namespace MountainWar.Service.Commons
{
  public class ServiceGeneric<TEntity> : Repository<TEntity> where TEntity : BaseEntity
  {
    public ServiceGeneric(DataContext context)
     : base(context)
    {
    }
  }
}
