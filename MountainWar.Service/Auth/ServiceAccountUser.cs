﻿using MongoDB.Bson;
using MountainWar.Core.Bussiness;
using MountainWar.Core.Enumns;
using MountainWar.Core.Interfaces;
using MountainWar.Core.Views;
using MountainWar.Data;
using MountainWar.Service.Commons;
using System;
using System.Linq;
using Tools;

namespace MountainWar.Service.Auth
{
  public class ServiceAccountUser : Repository<AccountUser>, IServiceAccountUser
  {
    private readonly ServiceGeneric<AccountUser> service;

    public ServiceAccountUser(DataContext context) : base(context)
    {
      service = new ServiceGeneric<AccountUser>(context);
    }

    public string NewAccount(ViewCreateAccount view)
    {
      try
      {
        var model = new AccountUser()
        {
          Name = view.Name,
          BirthDay = view.BirthDay,
          Mail = view.Mail,
          Password = EncryptServices.GetMD5Hash(view.Password),
          StartAccount = DateTime.Now,
          Status = EnumStatus.Enabled,
          Type = EnumTypeUser.Default
        };

        service.Insert(model);

        return "create";
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public string AlterAccount(ViewCreateAccount view, string id)
    {
      try
      {
        var model = service.GetAll(p => p._id == id).FirstOrDefault();

        model.Name = view.Name;
        model.BirthDay = view.BirthDay;
        model.Mail = view.Mail;
        service.Update(model, null);

        return "alter sucess";
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public string AlterPassword(ViewCreateAccount view, string id)
    {
      try
      {
        var model = service.GetAll(p => p._id == id).FirstOrDefault();
        model.Password = EncryptServices.GetMD5Hash(view.Password);
        service.Update(model, null);

        return "alter sucess";
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    public string AddCharacter(Character character, string id)
    {
      try
      {
        var model = service.GetAll(p => p._id == id).FirstOrDefault();
        model.Character = character.GetListView();
        service.Update(model, null);
        return "add ccharacter";
      }
      catch (Exception e)
      {
        throw e;
      }
    }

  }
}
