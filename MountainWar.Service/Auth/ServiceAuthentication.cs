﻿using MongoDB.Bson;
using MountainWar.Core.Bussiness;
using MountainWar.Core.Enumns;
using MountainWar.Core.Interfaces;
using MountainWar.Core.Views;
using MountainWar.Data;
using System.Linq;
using MountainWar.Service.Commons;
using MountainWar.Service.Specific;
using System;
using Tools;

namespace MountainWar.Service.Auth
{
  public class ServiceAuthentication : IServiceAuthentication
  {
    private readonly ServiceGeneric<AccountUser> service;
    private readonly ServiceLog serviceLog;

    public ServiceAuthentication(DataContext context)
    {
      service = new ServiceGeneric<AccountUser>(context);
      serviceLog = new ServiceLog(context);
    }

    public ViewUser Authentication(string mail, string password)
    {
      var user = service.GetAll(p => p.Status != EnumStatus.Disabled & p.Mail == mail && p.Password == EncryptServices.GetMD5Hash(password)).SingleOrDefault();

      if (user == null)
        throw new Exception("User/Password invalid");

      var userAuth = new ViewUser()
      {
        IdUser = user._id.ToString(),
        Name = user.Name,
        Mail = user.Mail,
        ChangePassword = user.ChangePassword,
        Photo = user.Photo,
        TypeUser = user.Type
      };

      LogSave(user);

      return userAuth;
    }

    public string Logoff(string id)
    {
      var user = service.GetAll(p => p._id == id).SingleOrDefault();

      var log = new ViewLog()
      {
        Description = "Logff",
        Local = "Authentication",
        Account = user.GetListView()
      };
      serviceLog.NewLog(log);

      return "lofoff";
    }

    public void LogSave(AccountUser user)
    {
      try
      {
        var log = new ViewLog()
        {
          Description = "Login",
          Local = "Authentication",
          Account = user.GetListView()
        };
        serviceLog.NewLog(log);
      }
      catch (Exception e)
      {
        throw e;
      }

    }

  }
}
