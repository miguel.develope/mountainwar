﻿using MongoDB.Bson;
using MongoDB.Driver;
using MountainWar.Core.Base;
using MountainWar.Core.Enumns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace MountainWar.Data
{
  public class Repository<T> where T : BaseEntity
  {
    public DataContext _context;
    IMongoCollection<T> _collection;
    Object _service;

    public Repository(DataContext context)
    {
      try
      {
        _context = context;
        _collection = context._db.GetCollection<T>(typeof(T).Name);
      }
      catch
      {
        throw;
      }
    }
    public Repository(DataContext context, Object service)
    {
      try
      {
        _context = context;
        _service = service;
        _collection = _context._db.GetCollection<T>(typeof(T).Name);
      }
      catch
      {
        throw;
      }
    }
    private IQueryable<T> CreateSet()
    {
      try
      {
        return _collection.AsQueryable<T>();
      }
      catch
      {
        throw;
      }
    }
    public T Insert(T entity)
    {
      try
      {
        entity._id = ObjectId.GenerateNewId().ToString();
        _collection.InsertOne(entity);

        return entity;
      }
      catch
      {
        throw;
      }
    }
    public void Update(T entity, FilterDefinition<T> filter)
    {
      try
      {
        if (filter == null)
        {

          filter = Builders<T>.Filter.Where(p => p._id == entity._id);
        }
        _collection.ReplaceOneAsync(filter, entity);
      }
      catch
      {
        throw;
      }
    }
    public long Delete(string id, bool logical = false)
    {
      try
      {
        DeleteResult resultDelete;
        if (logical)
        {
          var entity = GetAll(p => p._id == id).FirstOrDefault();
          entity.Status = EnumStatus.Disabled;
          var filter = Builders<T>.Filter.Where(p => p._id == entity._id);
          var result = _collection.ReplaceOneAsync(filter, entity);
          return result.Result.ModifiedCount;
        }
        else
        {
          resultDelete = _collection.DeleteOne(p => p._id == id);
          return resultDelete.DeletedCount;
        }
      }
      catch
      {
        throw;
      }
    }

    public T Single(Expression<Func<T, bool>> predicate = null)
    {
      try
      {
        var set = CreateSet();
        var query = (predicate == null ? set : set.Where(predicate));
        return query.SingleOrDefault();
      }
      catch
      {
        throw;
      }
    }
    public virtual IEnumerable<T> GetAll()
    {
      try
      {
        var result = _collection.AsQueryable<T>().Where(p => p.Status == EnumStatus.Enabled);
        return result;
      }
      catch
      {
        throw;
      }
    }
    public virtual IQueryable<T> GetAll(Expression<Func<T, bool>> filter)
    {
      try
      {
        var result = _collection.AsQueryable<T>().Where(p => p.Status == EnumStatus.Enabled).Where(filter);
        return result;
      }
      catch
      {
        throw;
      }
    }
    public int Count(Expression<Func<T, bool>> predicate = null)
    {
      try
      {
        var set = CreateSet();
        return (predicate == null ? set.Count() : set.Count(predicate));
      }
      catch
      {
        throw;
      }
    }
    public bool Exists(Expression<Func<T, bool>> predicate)
    {
      try
      {
        var set = CreateSet();
        return set.Any(predicate);
      }
      catch
      {
        throw;
      }
    }
  }
}
