﻿using MongoDB.Driver;
using MountainWar.Core.Base;

namespace MountainWar.Data
{
  public class DataContext : BaseEntity
  {
    private IMongoClient _client;
    public IMongoDatabase _db;

    public DataContext(string dburl, string dbname)
    {
      _client = new MongoClient(dburl);
      _db = _client.GetDatabase(dbname);
    }

  }
}
