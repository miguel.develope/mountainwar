﻿using System;
using System.IO;
using System.Xml.Serialization;
using Tools.Data;

namespace Tools
{
  public static class XmlConnection
  {
    public static Config ReadConfig()
    {
      try
      {
        string fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"../Config.xml");

        if (!File.Exists(fileName))
        {
          WriteConfig(new Config()
          {
            BlobKey = "BlobKey",
            DataBase = "DataBase",
            SendGridKey = "SendGridKey",
            Server = "Server",
            TokenServer = "TokenServer",
          }, fileName);
        }
        XmlSerializer xs = new XmlSerializer(typeof(Config));
        using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
        {
          return (Config)xs.Deserialize(fs);
        }
      }
      catch (Exception)
      {
        throw;
      }
    }

    public static Config ReadVariablesSystem()
    {
      try
      {
        if (Environment.GetEnvironmentVariable("MOUNTAINWAR_SERVER", EnvironmentVariableTarget.Machine) != null)
        {
          if (Environment.GetEnvironmentVariable("MOUNTAINWAR_SERVER", EnvironmentVariableTarget.Machine) != "")
            return new Config()
            {
              BlobKey = Environment.GetEnvironmentVariable("MOUNTAINWAR_BLOBKEY", EnvironmentVariableTarget.Machine),
              DataBase = Environment.GetEnvironmentVariable("MOUNTAINWAR_DATABASE", EnvironmentVariableTarget.Machine),
              SendGridKey = Environment.GetEnvironmentVariable("MOUNTAINWAR_SENDGRIDKEY", EnvironmentVariableTarget.Machine),
              Server = Environment.GetEnvironmentVariable("MOUNTAINWAR_SERVER", EnvironmentVariableTarget.Machine),
              TokenServer = Environment.GetEnvironmentVariable("MOUNTAINWAR_TOKENSERVER", EnvironmentVariableTarget.Machine)
            };
        }
        else if (Environment.GetEnvironmentVariable("MOUNTAINWAR_SERVER", EnvironmentVariableTarget.Process) != null)
        {
          if (Environment.GetEnvironmentVariable("MOUNTAINWAR_SERVER", EnvironmentVariableTarget.Process) != "")
            return new Config()
            {
              BlobKey = Environment.GetEnvironmentVariable("MOUNTAINWAR_BLOBKEY", EnvironmentVariableTarget.Process),
              DataBase = Environment.GetEnvironmentVariable("MOUNTAINWAR_DATABASE", EnvironmentVariableTarget.Process),
              SendGridKey = Environment.GetEnvironmentVariable("MOUNTAINWAR_SENDGRIDKEY", EnvironmentVariableTarget.Process),
              Server = Environment.GetEnvironmentVariable("MOUNTAINWAR_SERVER", EnvironmentVariableTarget.Process),
              TokenServer = Environment.GetEnvironmentVariable("MOUNTAINWAR_TOKENSERVER", EnvironmentVariableTarget.Process)
            };
        }
        else if (Environment.GetEnvironmentVariable("MOUNTAINWAR_SERVER", EnvironmentVariableTarget.User) != null)
        {
          if (Environment.GetEnvironmentVariable("MOUNTAINWAR_SERVER", EnvironmentVariableTarget.User) != "")
            return new Config()
            {
              BlobKey = Environment.GetEnvironmentVariable("MOUNTAINWAR_BLOBKEY", EnvironmentVariableTarget.User),
              DataBase = Environment.GetEnvironmentVariable("MOUNTAINWAR_DATABASE", EnvironmentVariableTarget.User),
              SendGridKey = Environment.GetEnvironmentVariable("MOUNTAINWAR_SENDGRIDKEY", EnvironmentVariableTarget.User),
              Server = Environment.GetEnvironmentVariable("MOUNTAINWAR_SERVER", EnvironmentVariableTarget.User),
              TokenServer = Environment.GetEnvironmentVariable("MOUNTAINWAR_TOKENSERVER", EnvironmentVariableTarget.User)
            };
        }

        return ReadConfig();
        //return new Config()
        //{
        //  Server = "mongodb://MOUNTAINWAR:x14r53p5!a@127.0.0.1:27017/MOUNTAINWARinfra",
        //  ServerLog = "mongodb://MOUNTAINWAR:x14r53p5!a@127.0.0.1:27017/MOUNTAINWARinfra",
        //  ServerIntegration = "mongodb://MOUNTAINWAR:x14r53p5!a@127.0.0.1:27017/MOUNTAINWARinfra",
        //  DataBase = "MOUNTAINWARinfra",
        //  DataBaseLog = "MOUNTAINWARinfra",
        //  DataBaseIntegration = "MOUNTAINWARinfra",
        //};

      }
      catch (Exception)
      {
        throw;
      }
    }

    public static void WriteConfig(Config config, string fileName)
    {
      try
      {
        XmlSerializer xs = new XmlSerializer(typeof(Config));
        TextWriter writer = new StreamWriter(fileName);
        xs.Serialize(writer, config);
        writer.Close();
      }
      catch (Exception)
      {
        throw;
      }
    }
  }
}
