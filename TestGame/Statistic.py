from pymongo import MongoClient
import numpy as np
import pandas as pd
import sklearn as sk
from sklearn.model_selection import train_test_split
from sklearn import tree
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score

client = MongoClient("mongodb://analisa:bti9010@10.0.0.16:27017/analisatest")

db = client["analisatest"]

onboardings = db["OnBoarding"].find()
print(onboardings)

i = 0
listsex = []

for item in onboardings:
   sex = 0
   person = item["Person"]
   if person != None:
       personview = db["Person"].find_one({"_id":person['_id']})
       if personview != None:
           user = personview['User']
           if user != None:
                sex = user['Sex']
   listsex.append(sex)
   i = i + 1

database=pd.DataFrame(onboardings)
#database['sex'] = listsex
print(database)
user_predict = database.iloc[:,[1,2]]
user_response = database.iloc[:,[2]]

print(user_predict)

predict_train,predict_test,response_train,response_test = train_test_split(user_predict,user_response,
                                                    test_size=0.25,
                                                    random_state=33)

result = tree.DecisionTreeClassifier(criterion='gini',max_depth=1,
                                min_samples_leaf=5)

result = result.fit(predict_train,response_train)

predictions = result.predict(predict_test)
print(predictions)

#confusion_matrix(response_test, predictions)

print(accuracy_score(response_test, predictions))