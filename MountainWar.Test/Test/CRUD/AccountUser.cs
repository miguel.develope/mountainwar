﻿using MountainWar.Core.Bussiness;
using MountainWar.Core.Enumns;
using MountainWar.Service.Commons;
using MountainWar.Test.Commons;
using System;
using System.Linq;
using Tools;
using Xunit;

namespace MountainWar.Test.Test.CRUD
{
  /// <summary>
  /// Summary description for sTest
  /// </summary>

  public class TestPerson : TestCommons<AccountUser>
  {

    private readonly ServiceGeneric<AccountUser> service;

    public TestPerson()
    {
      try
      {
        base.Init();
        service = new ServiceGeneric<AccountUser>(base.context);
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    [Fact]
    public void PersonCompleteTest()
    {
      try
      {
        // Criar classe padrão
        var model = new AccountUser();
        model.Name = "Miguel Eduardo da Silva";
        model.Mail = "miguel.develope@gmail.com.br";
        model.Password = EncryptServices.GetMD5Hash("bti9010");
        model.Status = EnumStatus.Enabled;
        model.Type = EnumTypeUser.Default;
        model.StartAccount = DateTime.Now;
        model.ChangePassword = EnumChangePassword.No;

        // Insert
        this.service.Insert(model);


        var result = this.service.GetAll(p => p.Mail == "miguel.develope@gmail.com.br").SingleOrDefault();


        // Update
        result.Type = EnumTypeUser.God;
        this.service.Update(result, null);

        // Delete
       //this.service.Delete(result._id, false);
      }
      catch (Exception e)
      {
        throw e;
      }
    }

  }
}
