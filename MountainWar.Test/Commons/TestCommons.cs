﻿using MountainWar.Core.Bussiness;
using MountainWar.Data;
using MountainWar.Service.Commons;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MountainWar.Test.Commons
{
  public abstract class TestCommons<TEntity> : IDisposable
  {
    public DataContext context;
    public ServiceGeneric<AccountUser> serviceAccount;

    public void Dispose()
    {

      GC.SuppressFinalize(this);
    }

    protected void Init()
    {
      try
      {
        this.context = new DataContext("mongodb://admin:bti9010@10.0.0.14:27017/mountainwar", "mountainwar");
        this.serviceAccount = new ServiceGeneric<AccountUser>(context);

        var user = this.serviceAccount.GetAll(p => p.Mail == "miguel.develope@gmail.com.br").FirstOrDefault();
      }
      catch (Exception e)
      {
        throw e;
      }
    }

    protected void InitOffAccount()
    {
      this.context = new DataContext("mongodb://admin:bti9010@10.0.0.14:27017/mountainwar", "mountainwar");
    }

    public IList<ValidationResult> ValidateModel(object model)
    {
      var validationResults = new List<ValidationResult>();
      var ctx = new ValidationContext(model, null, null);
      Validator.TryValidateObject(model, ctx, validationResults, true);
      return validationResults;
    }

  }
}
